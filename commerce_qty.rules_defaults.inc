<?php

/**
 * @file
 * Default rule configurations for Commerce qty.
 */
function commerce_qty_default_rules_configuration() {
  $rules_max_order_quantity = '{ "rules_max_order_quantity" : {
    "LABEL" : "max order quantity",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [ "rules", "commerce_cart" ],
    "ON" : [ "commerce_cart_product_add" ],
    "IF" : [
      { "data_is" : {
          "data" : [ "commerce-line-item:quantity" ],
          "op" : "\u003e",
          "value" : "5"
        }
      }
    ],
    "DO" : [
      { "drupal_message" : {
          "message" : "You tried to order [commerce-line-item:quantity], the maximum quantity of 5 for [commerce-product:title] was added to your basket",
          "type" : "warning"
        }
      },
      { "data_set" : { "data" : [ "commerce-line-item:quantity" ], "value" : "5" } }
    ]
  }
}';
  $configs['rules_max_order_quantity'] = rules_import($rules_max_order_quantity);

  return $configs;
}
